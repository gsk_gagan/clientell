var svg = d3.select("#main_svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height");

// TODO : Change for different colors, can make it dynamic to read from json file
var color = d3.scaleLinear()
    .domain([0, 0.5, 0.6, 1])
    .range(["#28a745", "#ffc107", "#ea6d2c", "#dc3545"]);


var forceLink = d3.forceLink().id(function(d) { return d.id; }).distance(linkDistance)
var xForce = d3.forceX().x(function(d) {
                                            if (d.id == 0)
                                              return width/2;
                                            else if (d.id == 1)
                                              return width*.75;
                                            else if (d.id == 2)
                                              return width*.25;
                                            else if (d.id < 14)
                                              return width*.75;
                                            else
                                              return width*.25;
                                          });
// var forceLink = d3.forceLink().id(function(d) { return d.id; }).distance(function(d){ return 100; })
// var forceLink = d3.forceLink().id(function(d) { return d.id; })

var simulation = d3.forceSimulation()
    .force("link", forceLink)  // TODO - This needs to be obtained dynamically
    .force("charge", d3.forceManyBody().strength(-60))
    .force("center", d3.forceCenter(width / 2, height / 2))
    .force('x', xForce)
    .force('collision', d3.forceCollide().radius(function(d) { return d.size*1.5; }));
//set up the simulation and add forces  
// var simulation = d3.forceSimulation()
//           .nodes(nodes_data);
                              
// var link_force =  d3.forceLink(links_data)
//                         .id(function(d) { return d.name; });            
         
// var charge_force = d3.forceManyBody()
//     .strength(-100); 
    
// var center_force = d3.forceCenter(width / 2, height / 2);  
                      
// simulation
//     .force("charge_force", charge_force)
//     .force("center_force", center_force)
//     .force("links",link_force)
//  ;


// GSK: This is to be changed for each case
d3.json("./data/input_case2.json", function(error, graph) {
  if (error) throw error;

  var everything = svg.append("g")
                    .attr("class", "everything");

  var link = everything.append("g")
      .attr("class", "links") // Class name for the svg is "links"
    .selectAll("line")        // Select all lines (to be added below)
    .data(graph.links)        // Read all lines data from links
    .enter().append("line")   // Add one line and for each data read above and do the below
      .attr("stroke-width", lineWidth)  // The line attribute value is to be read from the data
      .attr("stroke-dasharray", lineType);

// Here we are just creating empty nodes (These are just placeholders, these will be used later on to fill the required data)
  var node = everything.append("g")  
      .attr("class", "nodes") // Class name for svg is "nodes"
    .selectAll("g")
    .data(graph.nodes)        
    .enter().append("g")    // Just create a new svg element for each data node
    
// Here we are taking the above set of nodes and performing our thing
  var circles = node.append("circle")
      .attr("class", circleClassType)
      .attr("r", radiusScaling)
      .attr("fill", radiusColor)
      .call(d3.drag()                 // We are just associating events with what is to happen if they are dragged. Can also have custom events.
          .on("start", dragstarted)
          .on("drag", dragged)
          .on("end", dragended));

// Adding the labels to each of the nodes (We are basically going to place the text (6,3) wrt the node center (or corner))
  var lables = node.append("text")
      .text(function(d) { return d.name; })
      .attr('x', function(d) { return radiusScaling(d) + 5; })
      .attr('y', function(d) { return 5; })
      .style('font-size', fontSize)
      .style('font-weight', fontWeight);

// To basically add tool tip for these
  node.append("title")
      .text(function(d) { return d.name; });

// TODO: Will have to understand this thing better
  simulation
      .nodes(graph.nodes)
      .on("tick", ticked);  // Call the following function

  simulation.force("link")
      .links(graph.links);

  function ticked() {
    link
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node
        .attr("transform", function(d) {
          return "translate(" + d.x + "," + d.y + ")";
        })
  }


  var zoom_handler = d3.zoom()
      .on("zoom", zoom_actions);

  zoom_handler(svg);    

  function zoom_actions(){
      everything.attr("transform", d3.event.transform)
  }

});

function dragstarted(d) {
  if (!d3.event.active) simulation.alphaTarget(0.3).restart();
  d.fx = d.x;
  d.fy = d.y;
}

function dragged(d) {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
}

function dragended(d) {
  if (!d3.event.active) simulation.alphaTarget(0);
  d.fx = null;
  d.fy = null;
}

///////////////////GENERIC FUNCTIONS///////////////////
function rangeMap(number, fromMin, fromMax, toMin, toMax) {
  return ((number-fromMin)/(fromMax-fromMin))*(toMax-toMin) + toMin;
}


///////////////////LINE FUNCTIONS///////////////////
function lineWidth(d) {
  fromMin = 0.0
  fromMax = 1.0
  toMin = 1.0
  toMax = 5.0
  return rangeMap(d.width, fromMin, fromMax, toMin, toMax);
}

function lineType(d) {
  if(d.type == 0)
    return "5,0"
  return "7,10";
}


///////////////////CIRCLE FUNCTIONS///////////////////
// TODO : Make better scaling to fix this
function radiusScaling(d) {
  // fromMin = 0
  // fromMax = 100.0*7.0/22.0
  // toMin = 10
  // toMax = 100
  // radiusInput = Math.sqrt(d.size*7.0/22.0)
  fromMin = 0
  fromMax = 100.0
  toMin = 10
  toMax = 50
  radiusInput = d.size
  return rangeMap(radiusInput, fromMin, fromMax, toMin, toMax);
}

// TODO : This thing is to be changed using our custom thing
function radiusColor(d) { 
  return color(d.color); 
}


function circleClassType(d) {
  classType = "type" + d.type
  if (d.color > 0.7)
    return "animate-flicker " + classType
  return classType
}

function fontSize(d) {
  fromMin = 0
  fromMax = 100
  toMin = 10
  toMax = 20
  return rangeMap(d.size, fromMin, fromMax, toMin, toMax)
}

function fontWeight(d) {
  if(d.id == 0)
    return "bold"
  return "normal"
}

var count = 0;
function linkDistance(d) { 
  return Math.max(150 - count++*5, 80); 
}