var svg = d3.select("svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height");

// var color = d3.scaleOrdinal(d3.schemeCategory20);
// var color = d3.scale.linear().domain([1,length])
//       .interpolate(d3.interpolateHcl)
//       .range([d3.rgb("#007AFF"), d3.rgb('#FFF500')]);
// var color = d3.scaleSequential(d3.interpolateInferno)
//     .domain([0, 1]);
var color = d3.scaleLinear()
    .domain([0, 0.5, 1])
    .range(["#dc3545", "#ffc107", "#28a745"]);   // TODO - Change the color values here to custom ones, can have the center point to different ones

var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().id(function(d) { return d.id; }).distance(function(d){ return 100; }))  // TODO - This needs to be obtained dynamically
    .force("charge", d3.forceManyBody())
    .force("center", d3.forceCenter(width / 2, height / 2));

d3.json("small_company.json", function(error, graph) {
  if (error) throw error;

  var link = svg.append("g")
      .attr("class", "links") // Class name for the svg is "links"
    .selectAll("line")        // Select all lines (to be added below)
    .data(graph.links)        // Read all lines data from links
    .enter().append("line")   // Add one line and for each data read above and do the below
      .attr("stroke-width", lineWidth)  // The line attribute value is to be read from the data
      .attr("stroke-dasharray", lineType);

// Here we are just creating empty nodes (These are just placeholders, these will be used later on to fill the required data)
  var node = svg.append("g")  
      .attr("class", "nodes") // Class name for svg is "nodes"
    .selectAll("g")
    .data(graph.nodes)        
    .enter().append("g")    // Just create a new svg element for each data node
    
// Here we are taking the above set of nodes and performing our thing
  var circles = node.append("circle")
      // .attr("r", 5)
      .attr("r", radiusScaling)
      .attr("fill", radiusColor) // TODO : This thing is to be changed using our custom thing
      .call(d3.drag()                 // We are just associating events with what is to happen if they are dragged. Can also have custom events.
          .on("start", dragstarted)
          .on("drag", dragged)
          .on("end", dragended));

// Adding the labels to each of the nodes (We are basically going to place the text (6,3) wrt the node center (or corner))
  var lables = node.append("text")
      .text(function(d) {
        return d.id;
      })
      .attr('x', function(d) { return radiusScaling(d) + 2; })
      .attr('y', function(d) { return 5; });

// To basically add tool tip for these
  node.append("title")
      .text(function(d) { return d.id; });

// TODO: Will have to understand this thing better
  simulation
      .nodes(graph.nodes)
      .on("tick", ticked);  // Call the following function

  simulation.force("link")
      .links(graph.links);

  function ticked() {
    link
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node
        .attr("transform", function(d) {
          return "translate(" + d.x + "," + d.y + ")";
        })
  }
});

function dragstarted(d) {
  if (!d3.event.active) simulation.alphaTarget(0.3).restart();
  d.fx = d.x;
  d.fy = d.y;
}

function dragged(d) {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
}

function dragended(d) {
  if (!d3.event.active) simulation.alphaTarget(0);
  d.fx = null;
  d.fy = null;
}

///////////////////GENERIC FUNCTIONS///////////////////
function rangeMap(number, fromMin, fromMax, toMin, toMax) {
  return ((number-fromMin)/(fromMax-fromMin))*(toMax-toMin) + toMin;
}


///////////////////LINE FUNCTIONS///////////////////
function lineWidth(d) {
  if (d.weight < 1.0) {
    return 1;
  }
  else if (d.weight > 4.0) {
    return 4.0;
  }
  return d.weight;
}

function lineType(d) {
  return "5,5";       // TODO - Change this to become a line once the thing starts working
}


///////////////////CIRCLE FUNCTIONS///////////////////
function radiusScaling(d) {
  fromMin = 0
  fromMax = 100
  // Using squareroots to convert areas to radius
  return rangeMap(Math.sqrt(d.size), Math.sqrt(fromMin), Math.sqrt(fromMax), 0, 50); 
}

function radiusColor(d) { 
  return color(d.conv); 
}