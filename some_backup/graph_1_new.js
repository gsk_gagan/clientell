var svg = d3.select("#main_svg"),
    width = +svg.attr("width"),
    height = +svg.attr("height");

// TODO : Change for different colors, can make it dynamic to read from json file
var color = d3.scaleLinear()
    .domain([0, 0.5, 1])
    .range(["#dc3545", "#ffc107", "#28a745"]);

// TODO : There are thing here which can be modified smartly
var simulation = d3.forceSimulation()
    .force("link", d3.forceLink().id(function(d) { return d.id; }).distance(function(d){ return 100; }))  // TODO - This needs to be obtained dynamically
    .force("charge", d3.forceManyBody())
    .force("center", d3.forceCenter(width / 2, height / 2));

// GSK: This is to be changed for each case
d3.json("./data/input_case1.json", function(error, graph) {
  if (error) throw error;

  var link = svg.append("g")
      .attr("class", "links") // Class name for the svg is "links"
    .selectAll("line")        // Select all lines (to be added below)
    .data(graph.links)        // Read all lines data from links
    .enter().append("line")   // Add one line and for each data read above and do the below
      .attr("stroke-width", lineWidth)  // The line attribute value is to be read from the data
      .attr("stroke-dasharray", lineType);

// Here we are just creating empty nodes (These are just placeholders, these will be used later on to fill the required data)
  var node = svg.append("g")  
      .attr("class", "nodes") // Class name for svg is "nodes"
    .selectAll("g")
    .data(graph.nodes)        
    .enter().append("g")    // Just create a new svg element for each data node
    
// Here we are taking the above set of nodes and performing our thing
  var circles = node.append("circle")
      .attr("r", radiusScaling)
      .attr("fill", radiusColor)
      .call(d3.drag()                 // We are just associating events with what is to happen if they are dragged. Can also have custom events.
          .on("start", dragstarted)
          .on("drag", dragged)
          .on("end", dragended));

// Adding the labels to each of the nodes (We are basically going to place the text (6,3) wrt the node center (or corner))
  var lables = node.append("text")
      .text(function(d) {
        return d.id;
      })
      .attr('x', function(d) { return radiusScaling(d) + 2; })
      .attr('y', function(d) { return 5; });

// To basically add tool tip for these
  node.append("title")
      .text(function(d) { return d.id; });  // TODO : Change it to d.name

// TODO: Will have to understand this thing better
  simulation
      .nodes(graph.nodes)
      .on("tick", ticked);  // Call the following function

  simulation.force("link")
      .links(graph.links);

  function ticked() {
    link
        .attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node
        .attr("transform", function(d) {
          return "translate(" + d.x + "," + d.y + ")";
        })
  }
});

function dragstarted(d) {
  if (!d3.event.active) simulation.alphaTarget(0.3).restart();
  d.fx = d.x;
  d.fy = d.y;
}

function dragged(d) {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
}

function dragended(d) {
  if (!d3.event.active) simulation.alphaTarget(0);
  d.fx = null;
  d.fy = null;
}

///////////////////GENERIC FUNCTIONS///////////////////
function rangeMap(number, fromMin, fromMax, toMin, toMax) {
  return ((number-fromMin)/(fromMax-fromMin))*(toMax-toMin) + toMin;
}


///////////////////LINE FUNCTIONS///////////////////
function lineWidth(d) {
  fromMin = 0.0
  fromMax = 1.0
  toMin = 1.0
  toMax = 5.0
  return rangeMap(d.width, fromMin, fromMax, toMin, toMax);
}

function lineType(d) {
  if(d.type == 0)
    return "5,0"
  return "5,5";
}


///////////////////CIRCLE FUNCTIONS///////////////////
function radiusScaling(d) {
  fromMin = 0
  fromMax = 100.0*7.0/22.0
  toMin = 10
  toMax = 100
  radiusInput = Math.sqrt(d.size*7.0/22.0)
  return rangeMap(radiusInput, fromMin, fromMax, toMin, toMax); 
}

// TODO : This thing is to be changed using our custom thing
function radiusColor(d) { 
  return color(d.color); 
}